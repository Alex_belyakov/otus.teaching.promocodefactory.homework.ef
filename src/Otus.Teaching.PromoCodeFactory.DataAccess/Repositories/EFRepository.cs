﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EFRepository<T> : IRepository<T> where T : BaseEntity
    {
        protected DbContext DataContext;

        public EFRepository(DbContext dataContext)
        {
            DataContext = dataContext;
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await DataContext.Set<T>().ToListAsync();
        }

        public async Task<List<T>> GetAllAsync(ISpecification specification)
        {
            var expression = specification.GetExpression();

            return await GetAll(expression);
        }

        public async Task<T> SingleOrDefaultAsync(ISpecification specification)
        {
            var expression = specification.GetExpression();
            var genericExpr = expression as Expression<Func<T, bool>>;

            return await DataContext.Set<T>().SingleOrDefaultAsync(genericExpr);
        }

        public async Task<List<T>> GetAllAsync(IEnumerable<ISpecification> specifications)
        {
            Expression commonExpression = Expression.Empty();

            foreach (var specification in specifications)
            {
                var expression = specification.GetExpression();
                commonExpression = Expression.And(commonExpression, expression);
            }

            return await GetAll(commonExpression);
        }

        private async Task<List<T>> GetAll(Expression expression)
        {
            var genericExpr = expression as Expression<Func<T, bool>>;

            return await DataContext.Set<T>().Where(genericExpr).ToListAsync();
        }

        public virtual async Task<T> GetByIdAsync(Guid id)
        {
            return await DataContext.Set<T>().SingleOrDefaultAsync(t => t.Id == id);
        }

        public async Task<List<T>> GetByIdAsync(IEnumerable<Guid> ids)
        {
            return await DataContext.Set<T>().Where(t => ids.Contains(t.Id)).ToListAsync();
        }

        public async Task AddAsync(T obj)
        {
            await DataContext.Set<T>().AddAsync(obj);
        }

        public async Task DeleteAsync(Guid id)
        {
            var objToDelete = await DataContext.Set<T>().SingleOrDefaultAsync(t => t.Id == id);

            if (objToDelete != null)
            {
                DataContext.Set<T>().Remove(objToDelete);
            }
        }

        public async Task DeleteAsync(IEnumerable<Guid> ids)
        {
            var objToDelete = await GetByIdAsync(ids);

            DataContext.Set<T>().RemoveRange(objToDelete);
        }

        public virtual async Task UpdateAsync(T obj)
        {
            DataContext.Set<T>().Update(obj);
        }
    }
}

﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private DataContext _dataContext;

        public IRepository<Customer> CustomerRepository => new EFCustomerRepository(_dataContext);

        public IRepository<Preference> PreferenceRepository => CreateRepository<Preference>();

        public IRepository<PromoCode> PromoCodeRepository => CreateRepository<PromoCode>();

        public IRepository<Employee> EmployeeRepository => CreateRepository<Employee>();

        public IRepository<Role> RoleRepository => CreateRepository<Role>();

        public UnitOfWork(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task CommitAsync()
        {
            await _dataContext.SaveChangesAsync();
        }

        private IRepository<T> CreateRepository<T>() where T : BaseEntity
        {
            return new EFRepository<T>(_dataContext);
        }

        public void Dispose()
        {
            _dataContext.Dispose();
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EFCustomerRepository : EFRepository<Customer>
    {
        public EFCustomerRepository(DbContext dataContext) : base(dataContext)
        {
        }

        public override async Task<Customer> GetByIdAsync(Guid id)
        {
            return await DataContext.Set<Customer>().Include(c => c.Preferences).Include(c => c.Promocodes).SingleOrDefaultAsync(t => t.Id == id);
        }

        public override async Task UpdateAsync(Customer obj)
        {
            var objToUpdate = await DataContext.Set<Customer>().Include(c => c.Preferences).Include(c => c.Promocodes).SingleOrDefaultAsync(t => t.Id == obj.Id);
            objToUpdate.Email = obj.Email;
            objToUpdate.FirstName = obj.FirstName;
            objToUpdate.LastName = obj.LastName;
            objToUpdate.Preferences = obj.Preferences;
            objToUpdate.Promocodes = obj.Promocodes;
        }
    }
}

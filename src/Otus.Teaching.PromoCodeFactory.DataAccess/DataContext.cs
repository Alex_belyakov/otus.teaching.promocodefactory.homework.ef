﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class DataContext : DbContext
    {
        public DataContext()
        {
        }

        public DataContext(DbContextOptions options) : base(options)
        {
            ChangeTracker.LazyLoadingEnabled = false;
        }

        public DbSet<PromoCode> PromoCode { get; set; }
        public DbSet<Customer> Customer { get; set; }
        public DbSet<Preference> Preference { get; set; }
        public DbSet<Employee> Employee { get; set; }
        public DbSet<Role> Role { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>()
                .HasMany(c => c.Preferences)
                .WithMany(p => p.Customers)                
                .UsingEntity<Dictionary<string, object>>(
                    "CustomerPreferences",
                    j => j.HasOne<Preference>().WithMany().OnDelete(DeleteBehavior.Cascade),
                    j => j.HasOne<Customer>().WithMany().OnDelete(DeleteBehavior.Cascade)
                );

            modelBuilder.Entity<Customer>()
                .HasMany(c => c.Promocodes)
                .WithMany(p => p.Customers)
                .UsingEntity<Dictionary<string, object>>(
                    "CustomerPromocodes",
                    j => j.HasOne<PromoCode>().WithMany().OnDelete(DeleteBehavior.Cascade),
                    j => j.HasOne<Customer>().WithMany().OnDelete(DeleteBehavior.Cascade)
                );
        }
    }
}

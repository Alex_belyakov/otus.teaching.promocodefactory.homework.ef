﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public static class DbContextExtensions
    {
        public static void ResetTracking(this DbContext dbContext)
        {
            dbContext.ChangeTracker.Clear();
        }

        public static void ResetTracking<T>(this DbContext dbContext, IEnumerable<T> entities) where T : BaseEntity
        {
            foreach (var entity in entities)
            {
                var tracked = dbContext.Find<T>(entity.Id);
                dbContext.Entry(tracked).State = EntityState.Detached;
            }
        }
    }
}

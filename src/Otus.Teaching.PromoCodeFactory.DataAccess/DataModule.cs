﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public static class DataModule
    {
        public static void AddDataServices(this IServiceCollection services)
        {
            services.AddDbContext<DataContext>(options => 
                options.UseSqlite("Filename=PromoCodeFactoryDb.sqlite", b => b.MigrationsAssembly("Otus.Teaching.PromoCodeFactory.WebHost")));
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IDbInitializer, DbInitializer>();
        }
    }
}

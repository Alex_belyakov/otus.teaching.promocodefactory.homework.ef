﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class GetPreferenceByNameSpec : ISpecification
    {
        private readonly string _name;

        public GetPreferenceByNameSpec(string name)
        {
            _name = name;
        }

        public Expression GetExpression()
        {
            Expression<Func<Preference, bool>> expression = t => t.Name == _name;

            return expression;
        }
    }
}

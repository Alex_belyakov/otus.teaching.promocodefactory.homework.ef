﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class GetEmployeeByEmailSpec : ISpecification
    {
        private readonly string _mail;

        public GetEmployeeByEmailSpec(string mail)
        {
            _mail = mail;
        }

        public Expression GetExpression()
        {
            Expression<Func<Employee, bool>> expression = t => t.Email == _mail;

            return expression;
        }
    }
}

﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class GetCustomersByPreferenceNameSpec : ISpecification
    {
        private string _name;

        public GetCustomersByPreferenceNameSpec(string name)
        {
            _name = name;
        }

        public Expression GetExpression()
        {
            Expression<Func<Customer, bool>> expression = t => t.Preferences.Any(p => p.Name == _name);

            return expression;
        }
    }
}

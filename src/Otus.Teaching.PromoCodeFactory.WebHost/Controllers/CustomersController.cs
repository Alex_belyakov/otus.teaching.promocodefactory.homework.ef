﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        IUnitOfWork _unitOfWork;
        IRepository<Customer> _customerRepository;
        IRepository<Preference> _preferenceRepository;

        public CustomersController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _customerRepository = _unitOfWork.CustomerRepository;
            _preferenceRepository = _unitOfWork.PreferenceRepository;
        }

        /// <summary>
        /// Getting the full list of customers
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<CustomerShortResponse>>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();
            var customersResponse = customers.Select(customer => new CustomerShortResponse
            {
                Email = customer.Email, 
                FirstName = customer.FirstName, 
                Id = customer.Id, 
                LastName = customer.LastName
            }).ToList();

            return Ok(customersResponse);
        }
        
        /// <summary>
        /// Getting the customer info
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null)
            {
                return NotFound();
            }

            var promoCodes = customer.Promocodes.Select(code =>
                new PromoCodeShortResponse
                {
                    BeginDate = code.BeginDate.ToString(),
                    Code = code.Code,
                    EndDate = code.EndDate.ToString(),
                    Id = code.Id,
                    PartnerName = code.PartnerName,
                    ServiceInfo = code.ServiceInfo
                }
            ).ToList();
            var preferences = customer.Preferences.Select(preference => 
                new PreferenceShortResponse 
                { 
                    Name = preference.Name 
                }).ToList();
            var customerResponse = new CustomerResponse {
                Id = id,
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                PromoCodes = promoCodes, 
                Preferences = preferences
            };

            return Ok(customerResponse);
        }
        
        /// <summary>
        /// Creating a customer
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var preferences = await _preferenceRepository.GetByIdAsync(request.PreferenceIds);
            var customer = new Customer
            {
                Id = Guid.NewGuid(),
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Preferences = preferences
            };

            await _customerRepository.AddAsync(customer);
            await _unitOfWork.CommitAsync();

            return StatusCode(201);
        }
        
        /// <summary>
        /// Editing the customer
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null)
            {
                return NotFound();
            }

            var preferences = await _preferenceRepository.GetByIdAsync(request.PreferenceIds);
            var editedCustomer = new Customer
            {
                Id = id,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Preferences = preferences, 
                Promocodes = customer.Promocodes
            };

            await _customerRepository.UpdateAsync(editedCustomer);
            await _unitOfWork.CommitAsync();

            return Ok();
        }
        
        /// <summary>
        /// Deleting the customer including promocodes
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            await _customerRepository.DeleteAsync(id);
            await _unitOfWork.CommitAsync();

            return Ok();
        }
    }
}
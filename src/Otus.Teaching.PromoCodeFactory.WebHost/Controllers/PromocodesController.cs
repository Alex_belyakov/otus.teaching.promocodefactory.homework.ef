﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<PromoCode> _promoCodeRepository;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<Employee> _employeeRepository;

        public PromocodesController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _promoCodeRepository = unitOfWork.PromoCodeRepository;
            _customerRepository = unitOfWork.CustomerRepository;
            _preferenceRepository = unitOfWork.PreferenceRepository;
            _employeeRepository = unitOfWork.EmployeeRepository;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promoCodes = await _promoCodeRepository.GetAllAsync();
            var response = promoCodes.Select(
                p => new PromoCodeShortResponse { 
                    BeginDate = p.BeginDate.ToString(), 
                    Code = p.Code, 
                    EndDate = p.EndDate.ToString(), 
                    Id = p.Id, 
                    PartnerName = p.PartnerName, 
                    ServiceInfo = p.ServiceInfo }).ToList();

            return response;
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var spec = new GetCustomersByPreferenceNameSpec(request.Preference);
            var customersToIssue = await _customerRepository.GetAllAsync(spec);
            var prefSpec = new GetPreferenceByNameSpec(request.Preference);
            var preference = await _preferenceRepository.SingleOrDefaultAsync(prefSpec);
            
            if (preference == null)
            {
                return BadRequest("Preference not found");
            }

            var startDateTime = DateTime.Now;
            var endDateTime = startDateTime.AddYears(1);
            var partnerSpec = new GetEmployeeByEmailSpec(request.PartnerManager);
            var manager = await _employeeRepository.SingleOrDefaultAsync(partnerSpec);

            if (manager == null)
            {
                return BadRequest("Partner not found");
            }

            var promoCode = new PromoCode
            {
                Id = Guid.NewGuid(),
                Code = request.PromoCode,
                ServiceInfo = request.ServiceInfo,
                PartnerName = request.PartnerName,
                Preference = preference,
                BeginDate = startDateTime,
                EndDate = endDateTime,
                PartnerManager = manager
            };

            foreach (var customer in customersToIssue)
            {
                await _promoCodeRepository.AddAsync(promoCode);
                customer.Promocodes.Add(promoCode);
                await _customerRepository.UpdateAsync(customer);

                await _unitOfWork.CommitAsync();                
            }

            return Ok();
        }
    }
}
﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferenceController : ControllerBase
    {
        IUnitOfWork _unitOfWork;
        IRepository<Preference> _preferenceRepository;

        public PreferenceController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _preferenceRepository = _unitOfWork.PreferenceRepository;
        }

        /// <summary>
        /// retrieve the preferences list
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetPreferences()
        {
            var preferences = await _preferenceRepository.GetAllAsync();
            var response = preferences.Select(p => new PreferenceShortResponse { Name = p.Name }).ToList();

            return Ok(response);
        }
    }
}

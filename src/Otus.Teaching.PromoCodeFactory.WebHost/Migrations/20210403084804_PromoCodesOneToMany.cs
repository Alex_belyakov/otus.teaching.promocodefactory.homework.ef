﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Migrations
{
    public partial class PromoCodesOneToMany : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CustomerPromocodes");

            migrationBuilder.AddColumn<Guid>(
                name: "CustomersId",
                table: "PromoCode",
                type: "TEXT",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_PromoCode_CustomersId",
                table: "PromoCode",
                column: "CustomersId");

            migrationBuilder.AddForeignKey(
                name: "FK_PromoCode_Customer_CustomersId",
                table: "PromoCode",
                column: "CustomersId",
                principalTable: "Customer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PromoCode_Customer_CustomersId",
                table: "PromoCode");

            migrationBuilder.DropIndex(
                name: "IX_PromoCode_CustomersId",
                table: "PromoCode");

            migrationBuilder.DropColumn(
                name: "CustomersId",
                table: "PromoCode");

            migrationBuilder.CreateTable(
                name: "CustomerPromocodes",
                columns: table => new
                {
                    CustomersId = table.Column<Guid>(type: "TEXT", nullable: false),
                    PromocodesId = table.Column<Guid>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerPromocodes", x => new { x.CustomersId, x.PromocodesId });
                    table.ForeignKey(
                        name: "FK_CustomerPromocodes_Customer_CustomersId",
                        column: x => x.CustomersId,
                        principalTable: "Customer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CustomerPromocodes_PromoCode_PromocodesId",
                        column: x => x.PromocodesId,
                        principalTable: "PromoCode",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CustomerPromocodes_PromocodesId",
                table: "CustomerPromocodes",
                column: "PromocodesId");
        }
    }
}

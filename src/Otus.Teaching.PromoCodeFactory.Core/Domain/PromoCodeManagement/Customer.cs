﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        :BaseEntity
    {
        public Customer()
        {
            Promocodes = new List<PromoCode>();
            Preferences = new List<Preference>();
        }

        [MaxLength(50)]
        public string FirstName { get; set; }

        [MaxLength(50)]
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        [MaxLength(50)]
        public string Email { get; set; }

        public ICollection<PromoCode> Promocodes { get; set; }

        public ICollection<Preference> Preferences { get; set; }
    }
}
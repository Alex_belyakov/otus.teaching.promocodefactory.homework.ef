﻿using System.Linq.Expressions;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions
{
    public interface ISpecification
    {
        public Expression GetExpression();
    }
}

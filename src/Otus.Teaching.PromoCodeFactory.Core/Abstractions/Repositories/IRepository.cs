﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        Task<T> SingleOrDefaultAsync(ISpecification specification);

        Task<List<T>> GetAllAsync(ISpecification specification);

        Task<List<T>> GetAllAsync(IEnumerable<ISpecification> specifications);

        Task<IEnumerable<T>> GetAllAsync();
        
        Task<T> GetByIdAsync(Guid id);

        Task<List<T>> GetByIdAsync(IEnumerable<Guid> ids);

        Task AddAsync(T obj);

        Task DeleteAsync(Guid id);

        Task DeleteAsync(IEnumerable<Guid> ids);

        Task UpdateAsync(T obj);
    }
}